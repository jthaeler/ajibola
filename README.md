**This repository serves as the submission to my task**

The challenge questions can be viewed [here](https://docs.google.com/document/d/1md0U22KmmfT1HA8VzjYU4uqPy76cXgGBnJxLB1PJDuU/edit#heading=h.nqqi7xni5wsm).

---

## Challenge 1

This is for the two algorithm questions (string compression and router connections).

---

## Challenge 2

This is for the aben subscription challenge.
The deployed solution can be viewed at [https://aben-sub.herokuapp.com](https://aben-sub.herokuapp.com).


-- 
Submitted by [pro.ajibolaojo@gmail.com](mailto:pro.ajibolaojo@gmail.com).
